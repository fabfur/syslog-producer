package main

import (
	"errors"
	"fmt"
	"strconv"
)

// Define a custom type to use flag like "10k" -> "10000"
// See https://go.dev/src/flag/example_test.go?s=933:2300#L33
type suffixedNumber string

func (s *suffixedNumber) String() string {
	return fmt.Sprint(*s)
}

func (s *suffixedNumber) Set(value string) error {
	_, err := strconv.Atoi(value)
	if err == nil {
		// No suffix
		*s = suffixedNumber(value)
		return nil
	}

	// Is there a better way to do this?
	flagPrefix := value[:len(value)-1]
	flagSuffix := value[len(value)-1:]

	v, err := strconv.Atoi(flagPrefix)
	if err != nil {
		return err
	}

	switch flagSuffix {
	case "k", "K":
		*s = suffixedNumber(fmt.Sprint(v * 1000))
	case "m", "M":
		*s = suffixedNumber(fmt.Sprint(v * 1000000))
	default:
		return errors.New("Unknown suffix in " + value)
	}
	return nil

}
