# Syslog Producer

This is a very simple program to generate and send arbitrary number of syslog lines to a specific socket, with sleeping and batch options.

The code is neither smart or original but should be enough adequate for benchmarking.

# Build

`make dep`
`make`

# Run

Examples:

```bash
# Send 10.000 log lines in batch (10 lines per batch) with 10ms sleeping between each batch to a unix socket:
syslog-producer -s /dev/log -log "test message" -m 10K -b 10 -sleep 10 -progress

# Send 100.000 log lines in batch (100 lines per batch) with 5 ms sleeping between eatch batch to a udp socket and repeat for 5 times (total 50K lines)
syslog-producer -s 127.0.0.1:1221 -log "test message" -sleep 5 -m 100K -b 100 -p 5 -progress

# Send 10M log lines reading from file (if the files has less row, will be repeated)
syslog-producer -s 127.0.0.1:1221 -logfile ./sample.log -m 10M
```

Optionally you can spawn a simple UDP server to ingest (processed) logs and count received lines, in case you suspect any loss due to socket buffer size. Obviously you should configure your log processor (Benthos) to send log to the specified address/unix socket.

A configurable timeout starts when the first message is received and exits the server goroutine at `-listen-timeout` seconds.

Note that the received lines calculation is performed with a simple newline count, so it's all but accurate for anything slightly more complex.

Examples:

```
# Send 100k log to /dev/log and listen on 127.0.0.1:1234 for logs. Timeout is 5s
syslog-producer -s /dev/log -logfile -logfile ./sample.log -m 100k -b 10 -l 127.0.0.1:1234 -listen-timeout 5

# Send 100k log to 127.0.0.1:1221 and listen for logs on a custom unix socket, with default timeout (1s) with progressbar
syslog-producer -s 127.0.0.1:1221 -logfile ./sample.log -m 100k -b 10 -l /tmp/test.sock -progress
```
