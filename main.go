package main

import (
	"bufio"
	"container/ring"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/schollz/progressbar/v3"
)

var (
	// Some "global" vars
	err              error
	r                *ring.Ring
	receiveLineCount int
	receiveTime      time.Duration

	// flags
	passFlag            int
	batchSizeFlag       suffixedNumber = "10"
	maxRequestsFlag     suffixedNumber = "10000"
	sleepTime           int
	sendSocket          string
	listenSocket        string
	listenSocketTimeout int
	logMessage          string
	logFile             string
	showProgress        bool
)

func init() {
	flag.StringVar(&listenSocket, "l", "", "Listen socket address. Eg. /tmp/test.sock")
	flag.IntVar(&listenSocketTimeout, "listen-timeout", 1, "Listen socket timeout after last read, in seconds.")
	flag.Var(&batchSizeFlag, "b", "Log batch size (can use 'k' or 'm' as suffix).")
	flag.Var(&maxRequestsFlag, "m", "Max number of log lines to send (can use 'k' or 'm' as suffix)")
	flag.IntVar(&sleepTime, "sleep", 1000, "Sleep time between batches (in ms).")
	flag.IntVar(&passFlag, "p", 1, "Number of times the test is performed.")
	flag.StringVar(&sendSocket, "s", "/dev/log", "Socket address.")
	flag.StringVar(&logMessage, "log", "", "Log string to send")
	flag.StringVar(&logFile, "logfile", "", "Read log lines to send from the specified log file")
	flag.BoolVar(&showProgress, "progress", false, "Show progress while performing writes. Default: false")
}

func main() {
	var totalDuration time.Duration
	var totalSentLines int

	flag.Parse()

	// Convert these flags to numeric values
	batchSize, err := strconv.Atoi(batchSizeFlag.String())
	if err != nil {
		panic(err)
	}
	maxRequests, err := strconv.Atoi(maxRequestsFlag.String())
	if err != nil {
		panic(err)
	}

	var wg sync.WaitGroup
	if listenSocket != "" {
		wg.Add(1)
		fmt.Printf("[*] Wait 1s to allow listening on %s\n", listenSocket)
		go socketServer(listenSocket, &wg)
		time.Sleep(1 * time.Second)
	}

	// In case we have only one line to send (specified with -log "xxxx")
	// we create a ring anyway for simplicity
	if logMessage != "" {
		r = ring.New(1)
		r.Value = logMessage
	} else if logFile != "" {
		r, err = getLines(logFile)
		if err != nil {
			panic(err)
		}
	} else {
		fmt.Println("[!] Pass either -logfile or -log")
		os.Exit(1)
	}

	fmt.Printf("[*] Sending %d lines (in %d passes) (batch: %d, sleep: %d ms) to %s\n",
		maxRequests*passFlag, passFlag, batchSize, sleepTime, sendSocket)

	wallStart := time.Now()
	for passCounter := 0; passCounter < passFlag; passCounter++ {
		passStart := time.Now()

		sentLines, summedDuration, err := worker(maxRequests, batchSize)
		if err != nil {
			panic(err)
		}

		passWallDuration := time.Now().Sub(passStart)
		passMeanDuration := float32(summedDuration.Milliseconds()) / float32(sentLines)

		fmt.Printf("[%d] Sent %d lines in %v (wall: %v) (batch size: %d, req mean duration: %v ms)\n",
			passCounter,
			sentLines,
			summedDuration.Truncate(time.Millisecond),
			passWallDuration.Truncate(time.Millisecond),
			batchSize,
			passMeanDuration)
		totalSentLines += sentLines
		totalDuration += summedDuration
	}

	totalWallDuration := time.Now().Sub(wallStart)
	meanDuration := float32(totalDuration.Milliseconds()) / float32(totalSentLines)
	fmt.Printf("[*] Total lines sent: %d\n    Total duration: %v (wall: %v)\n    %.1f req/s, req mean duration: %v ms\n",
		totalSentLines,
		totalDuration.Truncate(time.Millisecond),
		totalWallDuration.Truncate(time.Millisecond),
		float64(totalSentLines)/totalWallDuration.Seconds(),
		meanDuration)

	if listenSocket != "" {
		fmt.Printf("[*] Waiting for listener timeout...")
		wg.Wait()
		fmt.Printf("[*] Received %d lines in %v\n    %.1f req/s\n    Lost %d lines\n",
			receiveLineCount,
			receiveTime.Truncate(time.Millisecond),
			float32(receiveLineCount)/float32(receiveTime.Truncate(time.Millisecond)),
			totalSentLines-receiveLineCount)
	}
}

func worker(maxRequests int, batchSize int) (sentLines int, summedDuration time.Duration, err error) {
	// Actual function to send log to socket
	var i int
	// Using github.com/schollz/progressbar/v3
	bar := progressbar.NewOptions(
		maxRequests,
		progressbar.OptionSetElapsedTime(false),
		progressbar.OptionSetPredictTime(false),
		progressbar.OptionShowCount(),
		progressbar.OptionThrottle(300*time.Millisecond),
	)

	sendSocketType := "udp"
	if isUDS(sendSocket) {
		sendSocketType = "unixgram" // not really correct, better check
	}

	conn, err := net.Dial(sendSocketType, sendSocket)
	if err != nil {
		return 0, summedDuration, err
	}
	defer conn.Close()

	for sentLines < maxRequests {
		batchStart := time.Now()
		for i = 0; i < batchSize; i++ {

			fmt.Println("DEBUG", r.Value.(string))
			//fmt.Fprintf(conn, r.Value.(string))
			if _, err := conn.Write([]byte(r.Value.(string))); err != nil {
				log.Fatal(err)
			}
			r = r.Next()
			sentLines++
		}

		batchDuration := time.Now().Sub(batchStart)
		summedDuration += batchDuration

		if showProgress {
			bar.Add(batchSize)
		}
		time.Sleep(time.Duration(sleepTime) * time.Millisecond)
	}

	// Just for formatting
	if showProgress {
		fmt.Println()
	}

	return sentLines, summedDuration, nil
}

func getLines(fileName string) (*ring.Ring, error) {
	// Reads lines from fileName and return a ring object to loop over
	file, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// Can't populate ring directly as we need the ring size in order to
		// create one
		lines = append(lines, scanner.Text())
	}

	r := ring.New(len(lines))

	for i := 0; i < r.Len(); i++ {
		r.Value = lines[i]
		r = r.Next()
	}
	return r, nil
}

func isUDS(addr string) bool {
	return strings.HasPrefix(addr, "/")
}

func socketServer(socketAddr string, wg *sync.WaitGroup) {
	// Listen on UDP socket and count lines received
	// and time spent.

	var lineCount int = 0
	var socketType string = "udp"

	if isUDS(socketAddr) {
		socketType = "unixgram" // not really correct, better check
	}
	//fmt.Printf("[*] Listening on %s:%s\n", socketType, socketAddr)

	cn, err := net.ListenPacket(socketType, socketAddr)
	if err != nil {
		panic(err)
	}
	defer func() {
		cn.Close()
		if socketType == "unixgram" {
			fmt.Printf("[!] Removing %s\n", socketAddr)
			err := syscall.Unlink(socketAddr)
			if err != nil {
				panic(err)
			}
		}
		wg.Done()
	}()

	buf := make([]byte, 1024) //TODO: optimization
	timeout := time.Duration(listenSocketTimeout) * time.Second
	receiveStart := time.Now()
	for {
		n, _, err := cn.ReadFrom(buf)
		// Set a timeout from the last successful read
		cn.SetReadDeadline(time.Now().Add(timeout))
		if err != nil {
			if e, ok := err.(net.Error); !ok || !e.Timeout() {
				panic(err)
			}
			break
		}
		// debug
		//fmt.Printf("[D] Read %d bytes: %s\n", n, string(buf[:n]))
		// Count only newlines, not whole packet (or count is doubled for syslog)
		if string(buf[:n]) == "\n" {
			lineCount++
		}

	}
	fmt.Printf("timeout (%v)\n", timeout)
	receiveTime = time.Now().Sub(receiveStart)
	receiveLineCount = lineCount
}
